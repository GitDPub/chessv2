﻿namespace ChessV2
{
    using System;
    using System.Collections.Generic;

    public class Program
    {
        static string[] allY = { "8", "7", "6", "5", "4", "3", "2", "1" };
        static string[] allX = { "A", "B", "C", "D", "E", "F", "G", "H" };
        static string[] allYInv = { "1", "2", "3", "4", "5", "6", "7", "8" };

        public string[] AllY
        {
            get
            {
                return allY;
            }
            set
            {
                allY = value;
            }
        }

        public string[] AllX
        {
            get
            {
                return allX;
            }
            set
            {
                allX = value;
            }
        }

        public string[] AllYInv
        {
            get
            {
                return allYInv;
            }
            set
            {
                allYInv = value;
            }
        }

        static string[][] slashWhite =
        {
            new string[] { "A6", "B7", "C8" },
            new string[] { "A4", "B5", "C6", "D7", "E8" },
            new string[] { "A2", "B3", "C4", "D5", "E6", "F7", "G8" },
            new string[] { "B1", "C2", "D3", "E4", "F5", "G6", "H7" },
            new string[] { "D1", "E2", "F3", "G4", "H5" },
            new string[] { "F1", "G2", "H3" }
        };

        static string[][] backSlashWhite =
        {
            new string[] { "G8", "H7" },
            new string[] { "E8", "F7", "G6", "H5" },
            new string[] { "C8", "D7", "E6", "F5", "G4", "H3" },
            new string[] { "A8", "B7", "C6", "D5", "E4", "F3", "G2", "H1" },
            new string[] { "A6", "B5", "C4", "D3", "E2", "F1" },
            new string[] { "A4", "B3", "C2", "D1" },
            new string[] { "A2", "B1" },
        };

        static string[][] slashBlack =
        {
            new string[] { "A7", "B8" },
            new string[] { "A5", "B6", "C7", "D8" },
            new string[] { "A3", "B4", "C5", "D6", "E7", "F8" },
            new string[] { "A1", "B2", "C3", "D4", "E5", "F6", "G7", "H8" },
            new string[] { "C1", "D2", "E3", "F4", "G5", "H6" },
            new string[] { "E1", "F2", "G3", "H4" },
            new string[] { "G1", "H2" }
        };

        static string[][] backSlashBlack =
        {
            new string[] { "F8", "G7", "H6" },
            new string[] { "D8", "E7", "F6", "G5", "H4" },
            new string[] { "B8", "C7", "D6", "E5", "F4", "G3", "H2" },
            new string[] { "A7", "B6", "C5", "D4", "E3", "F2", "G1" },
            new string[] { "A5", "B4", "C3", "D2", "E1" },
            new string[] { "A3", "B2", "C1" }
        };

        static void Main()
        {
            int numberOfTests = Convert.ToInt32(Console.ReadLine());
            List<string> tests = new List<string>();
            for (int i = 1; i <= numberOfTests; i++)
            {
                string line = Console.ReadLine();
                tests.Add(line);
            }
            foreach (var test in tests)
            {
                Calculate(test, allX, allY, allYInv, slashWhite, backSlashWhite, slashBlack, backSlashBlack);
            }
        }

        public static void Calculate(string line, string[] allX, string[] allY, string[] allYInv, string[][] slashWhite, string[][] backSlashWhite, string[][] slashBlack, string[][] backSlashBlack)
        {
            string[] split = line.Split(new char[] { ' ' }, StringSplitOptions.None);

            string x1 = split[0];
            string y1 = split[1];
            string x2 = split[2];
            string y2 = split[3];

            int xx1 = getIndex(allX, x1);
            int xx2 = getIndex(allX, x2);
            int yy1 = getIndex(allY, y1);
            int yy2 = getIndex(allY, y2);
            int yy1I = getIndex(allYInv, y1);
            int yy2I = getIndex(allYInv, y2);

            var isWhite = (((xx1 + yy1) % 2) == 0);
            string[][] slash = isWhite ? slashWhite : slashBlack;
            string[][] backSlash = isWhite ? backSlashWhite : backSlashBlack;

            if ((x1 == x2) && (y1 == y2))
            {
                Console.WriteLine("0 " + x1 + " " + y1);
            }
            else if (((((xx1 + yy1) % 2) == 0) && (((xx2 + yy2) % 2) == 0)) || ((((xx1 + yy1) % 2) != 0) && (((xx2 + yy2) % 2) != 0)))
            {
                bool tf = singlePath(xx1, yy1, xx2, yy2, yy1I, yy2I);
                string r = string.Empty;
                if (tf)
                {
                    r = "1 " + x1 + " " + y1 + " " + x2 + " " + y2;
                }
                else
                {
                    string e = doublePath(x1, y1, xx2, yy2, yy2I, slash, allX, allY, allYInv);
                    if (e == string.Empty)
                    {
                        e = doublePath(x1, y1, xx2, yy2, yy2I, backSlash, allX, allY, allYInv);
                    }
                    r = "2 " + x1 + " " + y1 + " " + e[0] + " " + e[1] + " " + x2 + " " + y2;
                }
                Console.WriteLine(r);
            }
            else
            {
                Console.WriteLine("Impossible");
            }

        }

        public static string doublePath(string x1, string y1, int x2, int y2, int y2I, string[][] theSlash, string[] allX, string[] allY, string[] allYI)
        {
            string[] selected = { };
            int px;
            int py;
            int pyi;
            bool tf2;
            string res = string.Empty;

            foreach (string[] row in theSlash)
            {
                if (Array.Exists(row, element => element == "" + x1 + y1))
                {
                    selected = row;
                }
            }

            foreach (string e in selected)
            {
                px = getIndex(allX, e[0] + "");
                py = getIndex(allY, e[1] + "");
                pyi = getIndex(allYI, e[1] + "");
                tf2 = singlePath(px, py, x2, y2, pyi, y2I);
                if (tf2)
                {
                    res = e;
                }
            }
            return res;
        }

        public static bool singlePath(int x1, int y1, int x2, int y2, int y1I, int y2I)
        {
            bool right = inDiagonal(x1, y1, x2, y2);
            bool left = inDiagonal(x1, y1I, x2, y2I);
            return (right || left) ;            
        }

        public static bool inDiagonal(int x1, int y1, int x2, int y2)
        {
            bool num = ((x1 + y1) == (x2 + y2));
            return num;
        }

        public static int getIndex(string[] all, string find) {
            return Array.FindIndex(all, value => value == find);
        }

    }
}
