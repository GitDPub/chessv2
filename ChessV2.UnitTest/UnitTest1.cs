using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ChessV2.UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        Program Test;

        [TestInitialize]
        public void TestInitialize()
        {
            Test = new Program();
        }

        [TestCleanup]
        public void TestClean()
        {
            Test = null;
        }

        [TestMethod]
        public void allYHasProperValues()
        {
            CollectionAssert.Contains(Test.AllY, "8", "it does not have 8 as alphabetic value");
            CollectionAssert.Contains(Test.AllY, "7", "it does not have 7 as alphabetic value");
            CollectionAssert.Contains(Test.AllY, "6", "it does not have 6 as alphabetic value");
            CollectionAssert.Contains(Test.AllY, "5", "it does not have 5 as alphabetic value");
            CollectionAssert.Contains(Test.AllY, "4", "it does not have 4 as alphabetic value");
            CollectionAssert.Contains(Test.AllY, "3", "it does not have 3 as alphabetic value");
            CollectionAssert.Contains(Test.AllY, "2", "it does not have 2 as alphabetic value");
            CollectionAssert.Contains(Test.AllY, "1", "it does not have 1 as alphabetic value");
        }

        [TestMethod]
        public void allXHasProperValues()
        {
            string[] xs = { "A", "B", "C", "D", "E", "F", "G", "H" };
            CollectionAssert.Equals(Test.AllX, xs);
        }

        [TestMethod]
        public void allYInvHasProperValues()
        {
            string[] iInv = { "1", "2", "3", "4", "5", "6", "7", "8" };
            CollectionAssert.Equals(Test.AllYInv, iInv);
        }

        [TestMethod]
        public void allXHasNoUnproperValues()
        {
            CollectionAssert.DoesNotContain(Test.AllX, "I");
            CollectionAssert.DoesNotContain(Test.AllX, "J");
        }

        [TestMethod]
        public void allYHasNoUnproperValues()
        {
            CollectionAssert.DoesNotContain(Test.AllY, "0");
            CollectionAssert.DoesNotContain(Test.AllY, "9");
        }

        [TestMethod]
        public void allYInvHasNoUnproperValues()
        {
            CollectionAssert.DoesNotContain(Test.AllYInv, "0");
            CollectionAssert.DoesNotContain(Test.AllYInv, "9");
        }

        [TestMethod]
        public void getCorrectIndex()
        {
            string[] indexs = { "0", "1", "2", "3" };            
            Assert.AreEqual(Program.getIndex(indexs, "0"), 0);
            Assert.AreEqual(Program.getIndex(indexs, "1"), 1);
            Assert.AreEqual(Program.getIndex(indexs, "2"), 2);
            Assert.AreEqual(Program.getIndex(indexs, "3"), 3);
        }

        [TestMethod]
        public void isInDiagonal()
        {
            Assert.IsTrue(Program.inDiagonal(7,0,0,7));
        }

        [TestMethod]
        public void notInDiagonal()
        {
            Assert.IsFalse(Program.inDiagonal(0, 0, 1, 5));
        }

    }
}
